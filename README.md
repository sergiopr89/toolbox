# ToolBox

## Project description
This project is intended as a "toolbox" which includes (only) useful code snippets and very simple tools (i.e., a script to generate a PEM encoded keypair of self-signed certficates).
