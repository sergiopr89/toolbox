#!/bin/bash

# Base path for certificates
BASE_PATH=$(dirname $(readlink -f $0))/certs

# Get a name for certs and check it
NAME=$1
if [[ -z ${NAME} || $(echo ${NAME} | grep "/") ]]; then
    echo -e "Usage: $0 <certificate name>\n\tNote: It can't contain slashes"
    exit 1
fi

# Try to create an output dir
OUTPUT_PATH="${BASE_PATH}/${NAME}"
mkdir -p ${OUTPUT_PATH}

# Generate the keypair files
CERT_FILE="${OUTPUT_PATH}/${NAME}.cer"
CERT_KEY="${OUTPUT_PATH}/${NAME}.key"
openssl req -nodes -x509 -newkey rsa:2048 -keyout ${CERT_KEY} -out ${CERT_FILE} -days 365
RESULT=$?

if [[ $RESULT -ne 0 ]]; then
    echo "Couldn't generate your certificates :("
    exit $RESULT
fi

echo -e "Your certificates have been generated!\n\tCertificate: ${CERT_FILE}\n\tKey: ${CERT_KEY}"
